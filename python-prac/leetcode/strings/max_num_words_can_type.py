"""
There is a malfunctioning keyboard where some letter keys do not work.
All other keys on the keyboard work properly. Given a string text of words
separated by a single space (no leading or trailing spaces) and a string
brokenLetters of all distinct letter keys that are broken, return the
number of words in text you can fully type using this keyboard.

Example 1:
Input: text = "hello world", brokenLetters = "ad"
Output: 1
Explanation: We cannot type "world" because the 'd' key is broken.

Example 2:
Input: text = "leet code", brokenLetters = "lt"
Output: 1
Explanation: We cannot type "leet" because the 'l' and 't' keys are broken.

Example 3:
Input: text = "leet code", brokenLetters = "e"
Output: 0
Explanation: We cannot type either word because the 'e' key is broken.
"""

class Solution(object):
    def canBeTypedWords(self, text, brokenLetters):
        # Initialize counter to keep track of fully typable words
        counter = 0
        # Split text into individual words
        seperate_words = text.split()
        # Iterate through each word
        for word in seperate_words:
            # Assume each word can be fully typed
            fully_type = True
            # Iterate through each character in each word
            for char in word:
                # If any character is the same as broken letter change True to False
                if char in brokenLetters:
                    fully_type = False
            # If word can be fully typed increment counter
            if fully_type:
                counter += 1
        return counter
