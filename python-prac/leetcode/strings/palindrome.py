"""
Given an integer x, return true if x is a
palindrome, and false otherwise.

Example 1:
Input: x = 121
Output: true
Explanation: 121 reads as 121 from left to right and from right to left.
"""

class Solution(object):
    def isPalindrome(self, x):
        # Convert integer to string
        num_str = str(x)

        # Check if the string is equal to its reverse
        return num_str == num_str[::-1]
