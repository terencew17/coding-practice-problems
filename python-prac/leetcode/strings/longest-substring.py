"""
Given a string s, find the length of the longest
substring
 without repeating characters.

Example 1:
Input: s = "abcabcbb"
Output: 3
Explanation: The answer is "abc", with the length of 3.

Example 2:
Input: s = "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3.
Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.
"""

class Solution(object):
    def lengthOfLongestSubstring(self, s):
        n = len(s)
        if n <= 1:
            return n

        # Use a set to keep track of unique characters
        char_set = set()

        # Initialize pointers and variables
        left = 0
        right = 0
        max_length = 0

        # Start a loop until right pointer reaches end
        while right < n:
            # Check if char at right postion if in set
            if s[right] not in char_set:
                # add char to the set since it is unique
                char_set.add(s[right])
                # Update max length
                max_length = max(max_length, right - left + 1)
                # Move right pointer to next position
                right += 1
            else:
                # remove char at the left position from the set
                char_set.remove(s[left])
                # move left pointer to next position
                left += 1

        return max_length
