"""
Given two strings s and t, return true if t is an anagram of s, and false otherwise.
An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase,
typically using all the original letters exactly once.

Example 1:
Input: s = "anagram", t = "nagaram"
Output: true

Example 2:
Input: s = "rat", t = "car"
Output: false
"""

class Solution(object):
    def isAnagram(self, s, t):
        # if length of strings are not same return False
        if len(s) != len(t):
            return False
        # Create dict to count # of character
        count_one = {}
        count_two = {}
        # iterate thru string 1 and count characters
        for char in s:
            if char in count_one:
                count_one[char] += 1
            else:
                count_one[char] = 1
        # iterate thru string 2 and count character
        for char in t:
            if char in count_two:
                count_two[char] += 1
            else:
                count_two[char] = 1

        # true if string 1 = string 2
        return count_one == count_two

        # string_one = sorted(s)
        # string_two = sorted(t)

        # return string_one == string_two
