"""
You are given a large integer represented as an integer array digits, where each digits[i] is
the i'th digit of the integer. The digits are ordered from most significant to least significant
in left-to-right order. The large integer does not contain any leading 0's.
Increment the large integer by one and return the resulting array of digits.

Example 1:
Input: digits = [1,2,3]
Output: [1,2,4]
Explanation: The array represents the integer 123.
Incrementing by one gives 123 + 1 = 124.
Thus, the result should be [1,2,4].
"""

class Solution(object):
    def plusOne(self, digits):
        # initialize variable set to 1
        increment = 1
        # iterate through the digits from right to left
        for i in range(len(digits) - 1, -1, -1):
            # Add the increment to the current digit. If the sum is less than 10,
            # update the current digit with the sum and set the carry to 0.
            # If the sum is equal to 10, update the current digit to 0 and set the carry to 1.
            digits[i] += increment
            increment = digits[i] // 10
            digits[i] %= 10
        # if the increment is equal to  1, it means that a new digit
        # needs to be added so insert 1 at the beginning of the array.
        if increment == 1:
            digits.insert(0, 1)

        return digits
