"""
Given an array of integers nums containing n + 1 integers where each integer is in the range [1, n] inclusive.
There is only one repeated number in nums, return this repeated number.
You must solve the problem without modifying the array nums and uses only constant extra space.

Example 1:
Input: nums = [1,3,4,2,2]
Output: 2

Example 2:
Input: nums = [3,1,3,4,2]
Output: 3
"""

class Solution(object):
    def findDuplicate(self, nums):
        # Initialize the slow and fast pointers
        slow = nums[0]
        fast = nums[0]

        # Move both pointers until they meet inside the cycle
        while True:
            # Slow pointer moves one step at a time
            slow = nums[slow]
            # Fast pointer moves two steps at a time
            fast = nums[nums[fast]]
            # Break loop once both pointers meet inside the cycle
            if slow == fast:
                break

        # Move one pointer to the beginning of the array and keep the other at the meeting point
        slow = nums[0]
        while slow != fast:
            slow = nums[slow]
            fast = nums[fast]

        # Return the duplicate number
        return slow
