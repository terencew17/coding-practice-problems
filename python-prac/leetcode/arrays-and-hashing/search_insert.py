"""
Given a sorted array of distinct integers and a target value, return the index if
the target is found. If not, return the index where it would be if it were inserted in order.
You must write an algorithm with O(log n) runtime complexity.

Example 1:
Input: nums = [1,3,5,6], target = 5
Output: 2

Example 2:
Input: nums = [1,3,5,6], target = 2
Output: 1

Example 3:
Input: nums = [1,3,5,6], target = 7
Output: 4

"""

class Solution(object):
    def searchInsert(self, nums, target): # Binary Search
        # Initialize two pointers: left and right
        # left at the start of the array
        left = 0
        # Right at the end of the array
        right = len(nums) - 1
        # Create while loop that continues as long as left <= right
        while left <= right:
            # Find middle index
            mid = left + (right - left) // 2
            # If middle index is == target return mid index
            if nums[mid] == target:
                return mid
            # If midd index is less than target, update left pointer to mid + 1
            elif nums[mid] < target:
                left = mid + 1
            # If mid index is greater than target, update right pointer to mid - 1
            else:
                right = mid - 1
        return left

"""
Binary Search Algorithm:

Efficient algorithm for finding an item from a sorted list. Works by dividing the array repeatedly in half
to just one element remaining; time complexity O(logN)

Steps:
1. Start at the most left(first) and most right(last) element
2. Find middle index of list
3. If middle element matches, return the index of the item
4. If search element is less than middle element, move first pointer to middle index + 1 (right)
5. If search element is greater than middle element, move last point to middle index - 1 (left)
6. Repeat until middle element is equal to the search element
"""
