"""
Leetcode #2529
Given an array nums sorted in non-decreasing order, return the maximum between
the number of positive integers and the number of negative integers.
In other words, if the number of positive integers in nums is pos and the
number of negative integers is neg, then return the maximum of pos and neg.
Note that 0 is neither positive nor negative.

Example 1:
Input: nums = [-2,-1,-1,1,2,3]
Output: 3
Explanation: There are 3 positive integers and 3 negative integers. The maximum count among them is 3.

Example 2:
Input: nums = [-3,-2,-1,0,0,1,2]
Output: 3
Explanation: There are 2 positive integers and 3 negative integers. The maximum count among them is 3.

Example 3:
Input: nums = [5,20,66,1314]
Output: 4
Explanation: There are 4 positive integers and 0 negative integers. The maximum count among them is 4.
"""

class Solution(object):
    def maximumCount(self, nums):
        # Initialize two counters for positive and negative numbers
        pos_counter = 0
        neg_counter = 0
        # Iterate through the array nums
        for i in nums:
            # If current element is > 0, it is a positive number, increment positive counter
            if i > 0:
                pos_counter += 1
            # If current element is < 0, it is negative, increment negative counter
            elif i < 0:
                neg_counter += 1
        # Use max function to return the larger number of the two counts
        return max(pos_counter, neg_counter)
