"""
Given two integer arrays nums1 and nums2, return an array of their intersection.
Each element in the result must be unique and you may return the result in any order

Example 1:
Input: nums1 = [1,2,2,1], nums2 = [2,2]
Output: [2]

Example 2:
Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
Output: [9,4]
Explanation: [4,9] is also accepted.
"""

class Solution(object):
    def intersection(self, nums1, nums2):
        # Create empty list to store elements
        result = []
        # Iterate through the first list
        for i in nums1:
            # Check if element is in nums2 and if it is not in result
            if i in nums2 and i not in result:
                # Add element to empty list
                result.append(i)
        return result
