"""
Given an array of integers nums and an integer target, return indices of the
two numbers such that they add up to target. You may assume that each input would
have exactly one solution, and you may not use the same element twice.
You can return the answer in any order.

Example:
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
"""

class Solution(object):
    def twoSum(self, nums, target):
        number_dict = {}

        # Iterate through the array
        for i, num in enumerate(nums):
            # Calculate the number
            number = target - num

            # Check if the number exists in the dictionary
            if number in number_dict:
                # Return the indices of the two numbers
                return [number_dict[number], i]

            # Store the current number and its index in the dictionary
            number_dict[num] = i

        # No two numbers add up to the target
        return None
