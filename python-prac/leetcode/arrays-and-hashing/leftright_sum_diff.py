"""
Leetcode #2574
Given a 0-indexed integer array nums, find a 0-indexed integer array answer where:
answer.length == nums.length.
answer[i] = |leftSum[i] - rightSum[i]|.
Where:
leftSum[i] is the sum of elements to the left of the index i in the array nums.
If there is no such element, leftSum[i] = 0.
rightSum[i] is the sum of elements to the right of the index i in the array nums.
If there is no such element, rightSum[i] = 0.
Return the array answer.

Example 1:
Input: nums = [10,4,8,3]
Output: [15,1,11,22]
Explanation: The array leftSum is [0,10,14,22] and the array rightSum is [15,11,3,0].
The array answer is [|0 - 15|,|10 - 11|,|14 - 3|,|22 - 0|] = [15,1,11,22].

Example 2:
Input: nums = [1]
Output: [0]
Explanation: The array leftSum is [0] and the array rightSum is [0].
The array answer is [|0 - 0|] = [0].
"""

class Solution(object):
    def leftRightDifference(self, nums):
        # Calculate lenght of the array
        n = len(nums)
        # Set three variables, filled w/ 0 to the length of array
        left_sum = [0] * n
        right_sum = [0] * n
        answer = [0] * n

        # Set left sum to 0 since there are no elements to left of the first element
        left_sum[0] = 0
        # Calculate sum of elements to left of each index i in nums array
        # Starts from the second element [1], and iterates through n-1(last element)
        for i in range(1, n):
            left_sum[i] = left_sum[i - 1] + nums[i - 1]

        # Set right sum to 0
        right_sum[n - 1] = 0
        # Calculate the sum of elements to the right of each array at index i
        # Starts from second to last element and iterates backwards
        for i in range(n - 2, -1, -1):
            right_sum[i] = right_sum[i + 1] + nums[i + 1]

        # Calculate answer array
        for i in range(n):
            # Take abs value difference between the elements of left and right sum
            answer[i] = abs(left_sum[i] - right_sum[i])

        return answer
