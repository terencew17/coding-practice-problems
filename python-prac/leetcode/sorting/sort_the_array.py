"""
Given an array of integers nums, sort the array in ascending order and return it.
You must solve the problem without using any built-in functions in O(nlog(n))
time complexity and with the smallest space complexity possible.

Example 1:
Input: nums = [5,2,3,1]
Output: [1,2,3,5]
Explanation: After sorting the array, the positions of some numbers are not changed (for example, 2 and 3), while the positions of other numbers are changed (for example, 1 and 5).

Example 2:
Input: nums = [5,1,1,2,0,0]
Output: [0,0,1,1,2,5]
Explanation: Note that the values of nums are not necessairly unique.
"""

# Merge sort

class Solution(object):
    def sortArray(self, nums):
        if len(nums) > 1:
            # Calculate the the index of the middle element in list
            middle = len(nums) // 2
            # Create new list containing elements from start to middle of list
            left = nums[:middle]
            # Create new list containing elements from middle to end of list
            right = nums[middle:]
            # Call sortArray recursively to divide and sort left and right
            self.sortArray(left)
            self.sortArray(right)
            # Create counter to track the indices of left, right, and nums
            i = j = k = 0

            while i < len(left) and j < len(right):
                # Compare elements from left and right arrays
                if left[i] <= right[j]:
                    # If element in left is smaller or equal place it into nums[k]
                    nums[k] = left[i]
                    # Update counter
                    i += 1
                else:
                    nums[k] = right[j]
                    j += 1
                k += 1
            # Check if one of the subarrays still have remaining elements
            # If elements remain in left array, add to nums array at index k
            while i < len(left):
                nums[k] = left[i]
                i += 1
                k += 1
            # if elements remain in right array, add to nums array at index k
            while j < len(right):
                nums[k] = right[j]
                j += 1
                k += 1
        return nums
