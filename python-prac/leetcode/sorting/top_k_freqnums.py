"""
Leetcode #347

Given an integer array nums and an integer k, return the k most
frequent elements. You may return the answer in any order.

Example 1:
Input: nums = [1,1,1,2,2,3], k = 2
Output: [1,2]

Example 2:
Input: nums = [1], k = 1
Output: [1]

"""

class Solution(object):
    def topKFrequent(self, nums, k):
        counter = {}

        for num in nums:
            if num in counter:
                counter[num] += 1
            else:
                counter[num] = 1

        sorted_elements = sorted(counter.keys(), key=lambda x: counter[x], reverse=True)
        result = sorted_elements[:k]
        return result
