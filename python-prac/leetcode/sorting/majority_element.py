"""
Given an array nums of size n, return the majority element.

The majority element is the element that appears more than ⌊n / 2⌋ times.
You may assume that the majority element always exists in the array.

Example 1:
Input: nums = [3,2,3]
Output: 3

Example 2:
Input: nums = [2,2,1,1,1,2,2]
Output: 2
"""

class Solution(object):
    def majorityElement(self, nums):
        # Create dictionary to store frequency of each element
        count = {}

        # Iterate through the list
        for i in nums:
            # Check if current element is already a key in dictionary
            if i in count:
                # Increment value associated w/ element in dictionary
                count[i] += 1
            else:
                # If element not in dictionary add new key w/ value of 1
                count[i] = 1
            # Check if the frequency of element is > half of the length of nums
            if count[i] > len(nums) / 2:
                return i
