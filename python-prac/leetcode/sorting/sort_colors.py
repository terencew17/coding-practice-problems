"""
Given an array nums with n objects colored red, white, or blue, sort them in-place
so that objects of the same color are adjacent, with the colors in the order red, white, and blue.
We will use the integers 0, 1, and 2 to represent the color red, white, and blue, respectively.
You must solve this problem without using the library's sort function.

Example 1:
Input: nums = [2,0,2,1,1,0]
Output: [0,0,1,1,2,2]

Example 2:
Input: nums = [2,0,1]
Output: [0,1,2]
"""


class Solution(object):
    def sortColors(self, nums):
        # Initialize three pointers:
        # low points to position where next 0 should go
        # mid to traverse the array, starts at the beginning
        # high points to position where next 2 should go and starts at end
        low = 0
        mid = 0
        high = len(nums) - 1

        # Create while loop that continues until all elements are processed
        while mid <= high:
            # check if element at mid pointed is 0
            if nums[mid] == 0:
                # swap mid with element low which moves the 0 to left of array
                nums[low], nums[mid] = nums[mid], nums[low]
                # increment low and mid to move pointers one step to right
                low += 1
                mid += 1
            # if element at mid is 1
            elif nums[mid] == 1:
                # increment mid counter because 1 should be in the middle of array
                mid += 1
            # if element at mid is 2
            else:
                # swap it with the element at high
                nums[mid], nums[high] = nums[high], nums[mid]
                # decrement high and move it one step to the left
                high -= 1

        return nums
