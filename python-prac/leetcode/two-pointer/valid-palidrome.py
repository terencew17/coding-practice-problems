"""
A phrase is a palindrome if, after converting all uppercase letters into lowercase
letters and removing all non-alphanumeric characters, it reads the same forward
and backward. Alphanumeric characters include letters and numbers.

Given a string s, return true if it is a palindrome, or false otherwise.

Example 1:
Input: s = "A man, a plan, a canal: Panama"
Output: true
Explanation: "amanaplanacanalpanama" is a palindrome.

Example 2:
Input: s = "race a car"
Output: false
Explanation: "raceacar" is not a palindrome.
"""

class Solution(object):
    def isPalindrome(self, s):
        # Create variable to store new string with only alphanumeric char
        new_string = ""
        # Iterate through the string
        for i in s:
            # Check if character is alphanumeric
            if i.isalnum():
                # Add char to variable and lowercase
                new_string += i.lower()
        # return true if string is palidrome
        return new_string == new_string[::-1]
