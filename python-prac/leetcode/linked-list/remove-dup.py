"""
Given the head of a sorted linked list, delete all duplicates such that
each element appears only once. Return the linked list sorted as well.

Example 1:
Input: head = [1,1,2]
Output: [1,2]

Example 2:
Input: head = [1,1,2,3,3]
Output: [1,2,3]
"""

# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution(object):
    def deleteDuplicates(self, head):

        current = head

        # Iterate thru list if there is a list and next node, continue
        while current and current.next:
            # Check if curr node is the same as next node
            if current.val == current.next.val:
                # point the current node to the node after next
                current.next = current.next.next

            else:
                # Move to next node
                current = current.next

        return head
