"""
Leetcode 203
Given the head of a linked list and an integer val, remove all the nodes of the
linked list that has Node.val == val, and return the new head.

Example 1:
Input: head = [1,2,6,3,4,5,6], val = 6
Output: [1,2,3,4,5]

Example 2:
Input: head = [], val = 1
Output: []

Example 3:
Input: head = [7,7,7,7], val = 7
Output: []

"""

class Solution(object):
    def removeElements(self, head, val):
        # Create a while loop to check if head node exist and if it is same as val
        while head and head.val == val:
            # If condition is true, remove head node
            head = head.next
        # Set current to new head, could be next value if original head == val
        current = head
        # Create while loop to iterate through rest of list
        while current and current.next:
            # If the value is equal to val, remove it from list
            if current.next.val == val:
                current.next = current.next.next
            # Move pointer to the next node
            else:
                current = current.next

        return head
