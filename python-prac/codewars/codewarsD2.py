"""
Given an array of integers, find the one that appears an odd number of times.

There will always be only one integer that appears an odd number of times.

Examples
[1,1,2] should return 2, because it occurs 1 time (which is odd).
[0,1,0,1,0] should return 0, because it occurs 3 times (which is odd).
[1,2,2,3,3,3,4,3,3,3,2,2,1] should return 4, because it appears 1 time (which is odd).
"""

def find_it(seq):
	result = 0
	for i in seq:
		result ^= i
	return result

def find_it(seq):
    for i in seq:
        if seq.count(i) % 2 != 0:
            return i

"""
In this kata you will create a function that takes a list of non-negative integers
and strings and returns a new list with the strings filtered out.

Example
filter_list([1,2,'a','b']) == [1,2]
filter_list([1,'a','b',0,15]) == [1,0,15]
filter_list([1,2,'aasf','1','123',123]) == [1,2,123]
"""

def filter_list(l):
    x = []
    for i in l:
        if isinstance(i, int) :
            x.append(i)
    return x


"""
Write a function that takes in a string of one or more words, and returns
the same string, but with all five or more letter words reversed
(Just like the name of this Kata). Strings passed in will consist of only letters and spaces.
Spaces will be included only when more than one word is present.

Examples:
spinWords( "Hey fellow warriors" ) => returns "Hey wollef sroirraw"
spinWords( "This is a test") => returns "This is a test"
spinWords( "This is another test" )=> returns "This is rehtona test"
"""

def spin_words(sentence):
    words = sentence.split()

    for i in range(len(words)):
        if len(words[i]) >= 5:
            words[i] = words[i][::-1]

    return ' '.join(words)
