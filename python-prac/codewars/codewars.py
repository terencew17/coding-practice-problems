""""
Complete the solution so that it reverses all of the words within the string passed in.

Words are separated by exactly one space and there are no leading or trailing spaces.

Example(Input --> Output): "The greatest victory is that which requires no battle"
--> "battle no requires which that is victory greatest The"
"""

def reversed_words(sentence):
    words = sentence.split()    # use .split to split words in sentence
    reversed = words[-1::-1]    # slice the indexes (start, stop, step)
    return " ".join(reversed)   # return new string with quotes using .join


"""
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
Finish the solution so that it returns the sum of all the multiples of 3 or 5 below the number passed in.
Additionally, if the number is negative, return 0 (for languages that do have them).
Note: If the number is a multiple of both 3 and 5, only count it once.
"""
def solution(number):
    # total = 0                           # create a variable and set it to 0
    # for n in range(number):             # iterate through the number using range function
    #     if n % 3 == 0 or n % 5 == 0:    # if remainder = 0 when dividing by 3 or 5
    #         total += n                  # add that number to total
    # return total
    return sum(x for x in range(number) if x % 3 == 0 or x % 5 == 0)


"""
Write a function to convert a name into initials. This kata strictly takes two words with one space in between them.
The output should be two capital letters with a dot separating them.
It should look like this:
Sam Harris => S.H
patrick feeney => P.F
"""

def abbrev_name(name):
    first_name, last_name = name.split()
    initials = f"{first_name[0].upper()}.{last_name[0].upper()}"
    return initials

"""
Write a function which calculates the average of the numbers in a given list.

Note: Empty arrays should return 0.
"""

def find_average(numbers):
    if len(numbers) == 0:
        return 0
    total = sum(numbers)
    average = total / len(numbers)
    return average
