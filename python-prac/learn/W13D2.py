"""
The function find_lists_with_minimum_value takes in a list of lists. The function will consider all of the values
in all of the lists and find the minimum value. Then, it will return a list that contains the indexes of the lists
that contain the minimum value in increasing order.

For example, consider this input:
# INPUT
[
  [6, 4, 5, 2, 6, 3, 2],
  [3, 3, 3, 4],
  [4, 2, 3, 2, 4],
]

# OUTPUT
[0, 2]
If you consider the values in all of the lists, the minimum value is 2. The lists that contain the value 2 are the
ones at index 0 and index 2. That's what the function will return.

Here's another example:
# INPUT
[
  [7],
  [9],
  [8],
  [1],
]

# OUTPUT
[3]

The minimum value from all of the values in all of the lists is 1. The only list that contains that value is the
fourth list at index 3. That's what the function will return.
"""

def find_lists_with_minimum_value(lists):
    results = []
    # sets minimum to the first index of the lists, first index in the list
    minimum = lists[0][0]

    # iterate through the lists
    for i in lists:
        # iterate through each list
        for x in i:
            # if x is < minimum value, set minimum to x
            if x < minimum:
                minimum = x

    i = 0
    # while i < length of list
    while i < len(lists):
        # if minimum value is in the lists
        if minimum in lists[i]:
            # append index of the list
            results.append(i)
        # add 1 to the counter
        i += 1
    return results

"""
When babies babble, they say things like GAGAGOOGOO or BABABABA. For the purposes of this question,
we'll define a baby talk word to be any non-empty string of letters that can be divided into two
equal-length portions in such a way that the first portion is identical to the second.

Based on that definition, the following strings are words in baby talk:
GAGA, GOOGOO, BABA, GUBBAGUBBA, DOGGIEDOGGIE, FDSFDS, IWANTMOREMILKIWANTMOREMILK, and XX.

The following strings are not words in baby talk: BABAB, GAGOO, BA, DOGGIE, and X.

Complete the baby_talk function to find the longest substring consisting of baby talk, as defined above,
and return that length. In the test cases below, the longest baby talk string in each input string is underlined.

Input	                        Output
GOOGOOGAGA	                    6
BABABABA	                    8
PTHHPTHHBAGOOGOOGAGABOOOOO	    6
XYBABABABAXYX	                8
BABAGOOGOOGOOGOOGOOGOOBA	    18
NOBABYTALKHERE	                0
"""

def baby_talk(s):
    # find length of s
    length = len(s)
    # set a variable = 0
    max_length = 0
    for i in range(length):
        for j in range(i + 1, length):
            substring_length = j - i + 1
            # if substring length is even continue
            if substring_length % 2 == 0:
                # split the substring into two halves
                first_half = s[i: i + substring_length // 2]
                second_half = s[i + substring_length // 2: j + 1]
                # if the first half == second half
                if first_half == second_half:
                    # update the max length: max length + substring length
                    max_length = max(max_length, substring_length)
    return max_length
