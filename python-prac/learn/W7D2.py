"""
FizzBuzz is a children's game that helps them learn division. The players count up from 1
and if the number they get is divisible by 3, they should say "fizz" instead of the number.
If the number is divisible by 5, they should say "buzz." If the number is divisible by 3 and 5,
they should say "fizzbuzz."

The progression of words spoken by the children should be:
1, 2, fizz, 4, buzz, fizz, 7, 8, fizz, buzz, 11, fizz, 13, 14, fizzbuzz... up-to the maximum number.

Your mission, should you accept it, is to implement the function fizzbuzz(maximum), which will
return an array containing the expected responses for a game of FizzBuzz.

Example:
Input: 6
Output: ["1", "2", "fizz", "4", "buzz", "6"]
"""

def fizzbuzz(maximum):
    result = []
    for num in range(1, maximum + 1):
        if num % 3 == 0 and num % 5 == 0:
            result.append("fizzbuzz")
        elif num % 3 == 0:
            result.append("fizz")
        elif num % 5 == 0:
            result.append("buzz")
        else:
            result.append(str(num))
    return result

"""
In this problem, you'll implement a function that takes a list of numbers and
returns the sum of all of the even numbers in the list.

Example:
Input: [1,2,3,4,5,6]
Output: 12
"""

def sum_evens(numbers):
    total = 0
    for num in numbers:
        if num % 2 == 0:
            total += num
    return total
