"""
Given two lists, nums1 and nums2, return a list that contains all of the numbers that are in nums1
that are not in nums2. The output should be sorted from smallest to largest.

nums1	            nums2	        Output
[5, 2, 4, 3, 1]	    [4, 2]	        [1, 3, 5]
[1, 5, 4, 2, 3]	    [6]	            [1, 2, 3, 4, 5]
[4, 2, 5, 3, 1]	    [2,1, 3, 4, 5]	[]
[1, 4, 2, 5, 3]	    []	            [1, 2, 3, 4, 5]
[]	                anything	    []
"""
def find_list_difference(nums1, nums2):
    new = []
    # loop over nums1
    for i in nums1:
        # if it is not in nums2
        if i not in nums2:
            # append it to the new list
            new.append(i)
    # return sorted new list
    return sorted(new)

"""
Given a 2D integer list nums (a list of lists) where each list in nums is a non-empty array of distinct positive
integers, return the list of integers that are present in each array of nums sorted in ascending order.

Here are some examples:

Input: nums = [[3,1,2,4,5],[1,2,3,4],[3,4,5,6]]
Output: [3,4]
Explanation: The only integers present in each of nums[0] = [3,1,2,4,5], nums[1] = [1,2,3,4], and nums[2] = [3,4,5,6] are 3 and 4, so we return [3,4].

Input: nums = [[1,2,3],[4,5,6]]
Output: []
Explanation: There does not exist any integer present both in nums[0] and nums[1], so we return an empty list [].
"""

def intersection(nums):
    # set = []
    # for num in nums:
    #   set.append(set(num))
    new = [set(x) for x in nums]
    same = set.intersection(*new)

    return sorted(list(same))
