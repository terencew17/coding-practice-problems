""""
car = {
    "make": "",
    "model": "",
    "year": 1995,
    "type": "",          # types: truck, sedan, wagon, suv
    "color": "",         # any color...
    "vin": "",
    "max_weight": 4000,  # maximum recommend weight in pounds (lbs.)
    "length": 15.2,      # total length in feet
    "fuel": "gas",       # fuel types: gas, diesel, electric
    "mpg": 20,           # miles/gallon of gas or equiv for diesel and electric
}
This function performs a filter operation, that is, it returns a subset of the input
based on some filtering criteria.
Return a list of the dictionaries that have: "color" = "red"
"""
def only_the_red(cars):
    # results = []
    # for car in cars:
    #     if car["color"] == "red":
    #         results.append(car)
    # return results
    return [car for car in cars if car["color"] == "red"]
"""
This function performs a filter operation.

Return a list of all of the dictionaries that have:

"year" < 1980 AND "mpg" < 12
"""
def old_guzzlers(cars):
    # results = []
    # for car in cars:
    #     if car["year"] < 1980 and car["mpg"] < 12:
    #         results.append(car)
    # return results
    return [car for car in cars if car["year"] < 1980 and car["mpg"] < 12]

"""
This function performs a filter operation.

Return a list of all of the dictionaries that have:

"length" > 25 OR "max_weight" > 4000
"""
def big_ones(cars):
    # results = []
    # for car in cars:
    #     if car["length"] > 25 or car["max_weight"] > 4000:
    #         results.append(car)
    # return results
    return [car for car in cars if car["length"] > 25 or car["max_weight"] > 4000]

"""
This function, like the previous, takes a list of car dictionaries as an input.
However, instead of filtering the list, we're going to sort it by the make of the car.

If we have an input like this:
cars = [
    {"make": "ford", "model": 8, "year": 1955},
    {"make": "chevy", "model": 12, "year": 1964},
    {"make": "ford", "model": 11, "year": 1978},
    {"make": "chevy", "model": 19, "year": 2000},
]
your function will produce this as the output:
output = {
    "ford": [
        {"make": "ford", "model": 8, "year": 1955},
        {"make": "ford", "model": 11, "year": 1978},
    ],
    "chevy": [
        {"make": "chevy", "model": 12, "year": 1964},
        {"make": "chevy", "model": 19, "year": 2000},
    ]
}
"""
def sort_by_make(cars):
    results = {}
    for car in cars:
        make = car["make"]
        if make not in results:
            results[make] = []
        results[make].append(car)
    return results
