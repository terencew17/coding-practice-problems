""""
Write a function, bubble_up() that will do one pass through the list swapping elements
as needed to move the larger numbers toward the end of the list.

Example:
lst = [4,3,2,1]
print(bubble_up(lst)) # --> [3,2,1,4]
"""
def bubble_up(numbers):
    for i in range(len(numbers) - 1):
        if numbers[i] > numbers[i + 1]:
            numbers[i], numbers[i + 1] = numbers[i + 1], numbers[i]
    return numbers

""""
Write a function called bubble_down() that works the same as bubble_up() except that
it starts at the end of the list and works its way towards the beginning of the list.
This function will move smaller elements from the end of the list towards the beginning.

Example:
lst = [4,3,2,1]
print(bubble_down(lst)) # --> [1,4,3,2]
"""

def bubble_down(numbers):
    for i in range((len(numbers) - 1), 0, -1):
        if numbers[i] < numbers[i - 1]:
            numbers[i], numbers[i - 1] = numbers[i - 1], numbers[i]
    return numbers

""""
Write a function called cocktail_sort() that will move larger numbers towards the end while
moving smaller numbers towards the beginning. Combination of bubble_up and bubble_down

Example:
lst = [4,3,2,1]
print(bubble_down(lst)) # --> [1,2,3,4]
"""

def cocktail_sort(numbers):                                                 # bubble_up
    while True:                                                             # while conditional is True
        swap = False
        for i in range(len(numbers) - 1):                                   # for loop to iterate through lenghth of numbers - 1
            if numbers[i] > numbers[i+1]:                                   # if numbers in first idex is > item in next index
                (numbers[i], numbers[i+1]) = (numbers[i+1], numbers[i])     # set the indexes to swap
                swap = True
        if not swap:                                                        # if we didn't swap break out of loop                                                                         #  break out of the loop
            break

        for i in range((len(numbers) - 1), 0, -1):                          # bubble_down
            if numbers[i] < numbers[i - 1]:
                numbers[i], numbers[i - 1] = numbers[i - 1], numbers[i]
                swap = True
            if not swap:
                break

    return numbers


x = [1, 4, 32, 43, 41, 52, 212, -3]
print(cocktail_sort(x))
