"""
Implement the function below that will return a single dictionary that contains all of
the key/value pairs of the two input dictionaries.
If both inputs have the same key, then the key in d2 should override the key in d1.

Example:

d1 = {"a":1, "b":2}
d2 = {"b":"bbb", "c": "ccc"}
d3 = merge_dictionaries(d1, d2)
print(d3) # --> {"a":1, "b":"bbb", "c": "ccc"})
Hint: dict.copy() will make a copy of a dictionary
"""
def merge_dictionaries(d1, d2):
    d3 = d1.copy()                  # copy first dicitonary to new variable
    d3.update(d2)                   # update copy of dictionary with second dictionary
    return d3


"""
This function takes a list of dictionaries (items) and a list of fields (fields).
It should return a copy of items where each dictionary only contains the fields listed in fields.

Example:
items = [
    {"a": 1, "b":2, "c": 3},
    {"a":3, "size": 4},
    {"b": 5, "d": 7}
]
fields = ["a", "b"]
result = just_these_fields(items, fields)
print(result) # -->
# [
#     {"a": 1, "b": 2},
#     {"a":3},
#     {"b": 5}
# ]
"""
def just_fields(items, fields):
    result = []
    for i in items:                         # iterate through items
        new = {}                            # create a new dictionary to hold new items
        for field in fields:                # iterate through fields to see if properties matches
            if field in i:                  # if field exist in items
                new[field] = i.get(field)   # assign the value to new dictionary using .get (if there is no values it will return None)
                # new[field] = i[field]       this will return an error if there is no value, .get will return None
        result.append(new)                  # append the dictionary to result
    return result                           # return

"""
This function takes a dictionary and returns the sum of all of the summable values in it.
Here "summable" means any value that is:
an int or a float and is a string and isnumeric(). These should be cast to float()
Example:
dictionary = {
    "dog": 1,
    "number": "three",
    "size": "2",
    "heavy": True,
    "weight": 3.4,
}
total = sum_summables(dictionary)
print(total) # --> 7.4
"""
def sum_summables(dictionary):
    total = 0
    for value in dictionary.values():
        if isinstance(value, (int,float)): # if value is instance of an integer or float
            total += value                  # add value to total if it is an int or float
        elif isinstance(value, str):        #checks if value is instance of a string
            if value.isnumeric():           #if value is numeric
                total += float(value)       # convert value to float and add
    return total

"""
This is kind-of like the second problem, but a little different...
This function takes a list of dictionaries (items) and dictionary of filters (filters).
It should return a filtered copy of items with only the items that match any of the filters.

Example:

items = [
    {"color":"blue", "size":"small"},
    {"color":"red", "size":"small"},
    {"color":"purple", "size":"medium"},
    {"color":"green", "size":"large"},
]
filters = {
    "color": "blue",
    "size": "medium"
}

result = only_items_with(items, filters)
print(result) # --> [
    {"color":"blue", "size":"small"},
    {"color":"purple", "size":"medium"},
]
"""
def only_items_with(items, filters):
    result = []
    for item in items:                      # iterate through items
        keep = False                        # set a flag to False
        for key, value in filters.items():  # iterate through filters, use .items to pull key and valye
            if item.get(key) == value:      # if key of item is equal to value
                keep = True                 # set flag to True
                break                       # break out of the for loop and restart loop until it iterates through everything
        if keep:                            # if keep is True
            result.append(item)             # append item to result
    return result
