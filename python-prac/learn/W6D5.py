""""
Implement this function: decode_and_pluck

Inputs:

input_json: a JSON encoded string that, once decoded, should be a Python dictionary
field: a list of field names that we want to extract
The function should decode input_json into a dictionary and then pluck out the fields named in fields as a new dictionary to return.

Example:
decode_and_pluck('{"a": 1, "b": 2, "c": 3}', ["a", "c"])
# --> {"a": 1, c": 3}
"""
import json
def decode_and_pluck(input_json, fields):
    # load json string into a variable
    dict = json.loads(input_json)
    new_dict = {}
    # iterate
    for field in fields:
        # takes old dictionary to access the property with the key of the field value
        # and assigns it to new_dict
        new_dict[field] = dict[field]
    return new_dict

""""
The function dict_to_list takes a dictionary as its input and transforms the dictionary into a
list by adding its keys and values to the list.

Example:
dict_to_list({"a": 1, "b": 2})
# --> ["a", 1, "b", 2]
"""
def dict_to_list(the_dict):
    key_values = []                         # set an empty list

    for key,value in the_dict.items():      # iterate through the_dict with .items to return key/value
        key_values.append(key)              # add key to empty list
        key_values.append(value)            # add value to empty list
    return key_values

"""
The function list_to_dict takes a list as its input and transforms it into a dictionary,
such that each successive pair of values in the list becomes a key and value for the dictionary.

Example:

list_to_dict(["a", 1, "b", 2])
# --> {"a": 1, "b": 2}
"""
def list_to_dict(lst):
    dic = {}                                # set an empty dict
    for i in range(0, len(lst), 2):         # iterate through the list, grab the first value, stop at last, increment by 2
        k = lst[i]                          # set key to the first index of 1
        v = lst[i+1]                        # set value to the second index
        dic[k] = v                          # set key to value
    return dic
