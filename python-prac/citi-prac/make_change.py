"""
You are given an infinite supply of coins of each of denominations
D = {D0, D1, D2, D3, ...... Dn-1}. You need to figure out the total
number of ways W, in which you can make a change for value V using
coins of denominations from D. Print 0, if a change isn't possible.
"""


def countWaysToMakeChange(denominations, value):
    # Create variable and initalize it as a list of zeroes with length of value + 1
    dp = [0] * (value + 1)
    # Set the index of variable to 1 because there is exactly one way to make change for a value of 0
    dp[0] = 1
    # Iterate over each coin demon
    for denomination in denominations:
        # Iterate over the range from demon to value + 1
        # represents the values we want to compute the # of ways to make change
        for i in range(denomination, value + 1):
            # Update the number of ways to make change for value i
            dp[i] += dp[i - denomination]
    # return value
    return dp[value]
