"""
You have been given an integer array/list 'ARR' of size 'N'. It represents
an elevation map wherein 'ARR[i]; denotes the elevation of the 'ith' bar.
Print the total amount of rainwater that can  be trapped in these elevations
"""

# TWo pointer approach

def getTrappedWater(arr, n):

    # Initialize two pointers, left and right
    left = 0
    right = n - 1
    # Initialize maxRight and Left to track the max elevation from each side
    maxLeft = 0
    maxRight = 0
    # Initalize a variable set to 0 to keep track of the trapped water
    totalWater = 0

    # While loop, as long as left is <= right
    while left <= right:
        # Check if elevation left index <= to right index
        if arr[left] <= arr[right]:
            # Check if current elevation is > max left
            if arr[left] > maxLeft:
                # if condition meets, set max left to current elevation
                maxLeft = arr[left]
            else:
                # if not, subtract current elevation from max left and add to total
                totalWater += maxLeft - arr[left]
            # Move to next index
            left += 1
        # If left is > right
        else:
            # if current elevation is > max right
            if arr[right] > maxRight:
                # set max right to current elevation
                maxRight = arr[right]
            else:
                # if not, subtract current elevation from max right and add total
                totalWater += maxRight - arr[right]
            # decreases by one
            right -= 1

    return totalWater
