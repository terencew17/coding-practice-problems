// Leetcode #1281

// Given an integer number n, return the difference between the product of
// its digits and the sum of its digits.

// Example 1:
// Input: n = 234
// Output: 15
// Explanation:
// Product of digits = 2 * 3 * 4 = 24
// Sum of digits = 2 + 3 + 4 = 9
// Result = 24 - 9 = 15

// Example 2:
// Input: n = 4421
// Output: 21
// Explanation:
// Product of digits = 4 * 4 * 2 * 1 = 32
// Sum of digits = 4 + 4 + 2 + 1 = 11
// Result = 32 - 11 = 21

var subtractProductAndSum = function (n) {
  // Convert the integer to a string
  const numToString = n.toString();
  // Initialize variable to keep track of the product and sum of numbers
  let product = 1;
  let sum = 0;
  // Iterate through the string
  for (const i of numToString) {
    // Convert string back to integer
    const number = parseInt(i);
    // Multiply each integer by the product variable
    product *= number;
    // Get the sum of each integer
    sum += number;
  }
  // Subtract sum from product
  let result = product - sum;
  return result;
};
