// Leetcode #217
// Given an integer array nums, return true if any value appears at least
// twice in the array, and return false if every element is distinct.

// Example 1:
// Input: nums = [1,2,3,1]
// Output: true

// Example 2:
// Input: nums = [1,2,3,4]
// Output: false

// Example 3:
// Input: nums = [1,1,1,3,3,4,3,2,4,2]
// Output: true

var containsDuplicate = function (nums) {
  // Create a set to store unique elements
  const unique = new Set();
  // Iterate through the array nums
  for (let i of nums) {
    // If element is found in Set, it is a duplicate, return true
    if (unique.has(i)) {
      return true;
      // Else, add the element to the Set
    } else {
      unique.add(i);
    }
  }
  // If no duplicates are found return false
  return false;
};
